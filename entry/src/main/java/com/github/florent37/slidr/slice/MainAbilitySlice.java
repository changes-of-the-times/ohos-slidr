package com.github.florent37.slidr.slice;

import com.github.florent37.harmonyslidr.Slidr;
import com.github.florent37.slidr.ResourceTable;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Component;
import ohos.agp.utils.Color;

public class MainAbilitySlice extends AbilitySlice {
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);


        {
            final Slidr slidr = (Slidr) findComponentById(ResourceTable.Id_slideure);
            if (slidr != null) {
                slidr.setMax(500);
                slidr.addStep(new Slidr.Step("test", 250, new Color(0xFF007E90), Color.RED));
                slidr.setTextMax("max\nvalue");
                slidr.setTextMin("min\nvalue");
                slidr.setCurrentValue(300);
//                slidr.setListener(new Slidr.Listener() {
//                    @Override
//                    public void valueChanged(Slidr slidr, float currentValue) {
//
//                    }
//                });

                findComponentById(ResourceTable.Id_max).setClickedListener(new Component.ClickedListener() {
                    @Override
                    public void onClick(Component component) {
                        slidr.setMax(1000);
                    }
                });

                findComponentById(ResourceTable.Id_current).setClickedListener(new Component.ClickedListener() {
                    @Override
                    public void onClick(Component component) {
                        slidr.setCurrentValue(100);
                    }
                });
            }

        }
        {
            final Slidr slidr = (Slidr) findComponentById(ResourceTable.Id_slideure2);
            if (slidr != null) {
                slidr.setMax(5000);
                slidr.setCurrentValue(5000);
                slidr.addStep(new Slidr.Step("test", 1500, new Color(0xFF007E90), new Color(0xFF111111)));
            }
        }
        {
            final Slidr slidr = (Slidr) findComponentById(ResourceTable.Id_slideure_regions);
            if (slidr != null) {
                slidr.setMax(3000);
                slidr.setRegionTextFormatter(new Slidr.RegionTextFormatter() {
                    @Override
                    public String format(int region, float value) {
                        return String.format("region %d : %d", region, (int) value);
                    }
                });
                slidr.addStep(new Slidr.Step("test", 1500, new Color(0xFF007E90), new Color(0xFF111111)));
            }
        }
    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }
}
